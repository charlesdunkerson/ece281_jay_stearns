--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : top_basys3.vhd
--| AUTHOR(S)     : Capt Phillip Warner
--| CREATED       : 3/9/2018  MOdified by Capt Dan Johnson (3/30/2020)
--| DESCRIPTION   : This file implements the top level module for a BASYS 3 to 
--|					drive the Lab 4 Design Project (Advanced Elevator Controller).
--|
--|					Inputs: clk       --> 100 MHz clock from FPGA
--|							btnL      --> Rst Clk
--|							btnR      --> Rst FSM
--|							btnU      --> Rst Master
--|							btnC      --> GO (request floor)
--|							sw(15:12) --> Passenger location (floor select bits)
--| 						sw(3:0)   --> Desired location (floor select bits)
--| 						 - Minumum FUNCTIONALITY ONLY: sw(1) --> up_down, sw(0) --> stop
--|							 
--|					Outputs: led --> indicates elevator movement with sweeping pattern (additional functionality)
--|							   - led(10) --> led(15) = MOVING UP
--|							   - led(5)  --> led(0)  = MOVING DOWN
--|							   - ALL OFF		     = NOT MOVING
--|							 an(3:0)    --> seven-segment display anode active-low enable (AN3 ... AN0)
--|							 seg(6:0)	--> seven-segment display cathodes (CG ... CA.  DP unused)
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std
--|    Files     : MooreElevatorController.vhd, clock_divider.vhd, sevenSegDecoder.vhd
--|				   thunderbird_fsm.vhd, sevenSegDecoder, TDM4.vhd, OTHERS???
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


entity top_basys3 is
	port(

		clk     :   in std_logic; -- native 100MHz FPGA clock
		
		-- Switches (16 total)
		sw  	:   in std_logic_vector(15 downto 0);
		
		-- Buttons (5 total)
		btnC	:	in	std_logic;					  -- GO
		btnU	:	in	std_logic;					  -- master_reset
		btnL	:	in	std_logic;                    -- clk_reset
		btnR	:	in	std_logic;	                  -- fsm_reset
		--btnD	:	in	std_logic;			
		
		-- LEDs (16 total)
		led 	:   out std_logic_vector(15 downto 0);

		-- 7-segment display segments (active-low cathodes)
		seg		:	out std_logic_vector(6 downto 0);

		-- 7-segment display active-low enables (anodes)
		an      :	out std_logic_vector(3 downto 0)
	);
end top_basys3;

architecture top_basys3_arch of top_basys3 is 
  
	-- COMPONENTS --------------------------------------
    component clock_divider is
        generic ( constant k_DIV : natural := 2    ); -- How many clk cycles until slow clock toggles. Effectively, you divide the clk double this number (e.g., k_DIV := 2 --> clock divider of 4)
        port (     i_clk    : in std_logic;
                   i_reset  : in std_logic;           -- asynchronous
                   o_clk    : out std_logic           -- divided (slow) clock
        );
    end component clock_divider;
    
    component MooreElevatorController is
        Port ( i_clk     : in  STD_LOGIC;
               i_reset   : in  STD_LOGIC;
               i_stop    : in  STD_LOGIC;
               i_up_down : in  STD_LOGIC;
               o_floor   : out STD_LOGIC_VECTOR (3 downto 0);
               o_light_reset : out STD_LOGIC          
        );
    end component MooreElevatorController;
    
    component thunderbird_fsm is
        Port ( 	i_clk2, i_reset2 : in std_logic;
                i_left, i_right : in std_logic;
                o_lights_L : out std_logic_vector(5 downto 0);
                o_lights_R : out std_logic_vector(5 downto 0)
        );
     end component thunderbird_fsm;
     
    component sevenSegDecoder is 
      port(
        -- Identify input and output bits here
        i_D : in std_logic_vector(3 downto 0);
        o_S : out std_logic_vector(6 downto 0)
      );
    end component sevenSegDecoder;
    
    component TDM4 is
        generic ( constant k_WIDTH : natural  := 7); -- bits in input and output
        port ( i_CLK         : in  STD_LOGIC;
               i_RESET       : in  STD_LOGIC; -- asynchronous
               i_D3          : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               i_D2          : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               i_D1          : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               i_D0          : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               o_DATA        : out STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
               o_SEL         : out STD_LOGIC_VECTOR (3 downto 0)    -- selected data line (one-cold)
         );
      end component TDM4;
      
      component hexDecoder is
          Port ( i_floor  : in STD_LOGIC_VECTOR (3 downto 0);
                 o_tens    : out STD_LOGIC_VECTOR (3 downto 0);
                 o_ones    : out STD_LOGIC_VECTOR (3 downto 0)       
           );
      end component hexDecoder;
      
      component destination_fsm is
          Port ( i_clk     : in  STD_LOGIC;
                 i_reset   : in  STD_LOGIC;
                 i_go      : in  STD_LOGIC;
                 i_dest1   : in  STD_LOGIC_VECTOR (3 downto 0);
                 i_dest2   : in  STD_LOGIC_VECTOR (3 downto 0);
                 i_floor   : in  STD_LOGIC_VECTOR (3 downto 0);
                 o_control : out STD_LOGIC_VECTOR (1 downto 0)   
               );
      end component;
    
    -- SIGNALS ------------------------------------------
    signal w_clk, w_clk2, w_clk3, w_down, w_resetE, w_resetC, w_resetL : std_logic;
    signal w_control: std_logic_vector (1 downto 0);
    signal w_floor, w_tens, w_ones: std_logic_vector (3 downto 0);
    signal w_tenTDM, w_oneTDM, w_7SD_EN_n : std_logic_vector (6 downto 0);
    
begin
    ----------------------------------------------------------------------------------------------------------------------------------------------------
	-- PORT MAPS ---------------------------------------------------------------------------------------------------------------------------------------
    clkdiv_inst : clock_divider 		--instantiation of clock_divider for elevator controller FSM
        generic map( k_DIV => 25000000) -- 2 Hz clock from 100 MHz
        port map   ( i_clk   => clk,
                     i_reset => w_resetC,
                     o_clk   => w_clk
        ); 
        
    clkdiv_lights_inst : clock_divider --instantiation of clock_divider for lights FSM
        generic map( k_DIV => 5000000 )-- 10 Hz clock from 100 MHz
        port map(    i_reset => w_resetC,
                     i_clk => clk,
                     o_clk => w_clk2
        );
    
    clkdiv_TDM_inst : clock_divider --instantiation of clock_divider for lights FSM
        generic map( k_DIV => 100000 )-- 50MHz clock from 100 MHz
        port map(    i_reset => w_resetC,
                     i_clk => clk,
                     o_clk => w_clk3
        );
    
    MooreElevatorController_inst : MooreElevatorController
        port map( i_clk         => w_clk,
                  i_reset       => w_resetE,
                  i_stop        => w_control(0),
                  i_up_down     => w_control(1),
                  o_floor       => w_floor,  
                  o_light_reset => w_resetL       
        );
        
    sevenSegDecoder_tens_inst : sevenSegDecoder
        port map( i_D => w_tens,
                  o_S => w_tenTDM
        );
        
    sevenSegDecoder_ones_inst : sevenSegDecoder
        port map( i_D => w_ones,
                  o_S => w_oneTDM
        );
        
	thunderbird_fsm_inst : thunderbird_fsm
	   port map(
	       i_clk2     => w_clk2,
	       i_reset2   => w_resetL,
	       i_left     => w_control(1),
	       i_right    => w_down,
	       o_lights_L => led (15 downto 10),
	       o_lights_R => led (5 downto 0)
	   );
	   
    TDM4_inst : TDM4
       generic map ( k_WIDTH => 7) -- bits in input and output
       port map ( i_CLK   => w_clk3,
                  i_RESET => w_resetE,
                  i_D3    => w_7SD_EN_n,
                  i_D2    => w_7SD_EN_n,
                  i_D1    => w_tenTDM,
                  i_D0    => w_oneTDM,
                  o_DATA  => seg,
                  o_SEL   => an
       );
       
    hexDecoder_inst : hexDecoder
        port map ( i_floor => w_floor,
                   o_tens  => w_tens,
                   o_ones  => w_ones 
        );
        
     destination_fsm_inst : destination_fsm 
        port map( i_clk     => w_clk,
                  i_reset   => w_resetE,
                  i_go      => btnC,
                  i_dest1   => sw (3 downto 0),
                  i_dest2   => sw (15 downto 12),
                  i_floor   => w_floor,  
                  o_control => w_control       
        );
	-------------------------------------------------------------------------------------------------------------------------------------------------------
	-- CONCURRENT STATEMENTS ------------------------------------------------------------------------------------------------------------------------------
	-- configure master reset functionality
	w_resetC <= btnU or btnL;
	w_resetE <= btnU or btnR;
	-- configure signals for thunderbirdFSM
	w_down <= not w_control(1);
	-- ground unused LEDs (which is all of them for Minimum functionality)
	led(9 downto 6) <= (others => '0');
	-- Wire up active high signal to turn off 7 segment display for ports 3 and 4
    w_7SD_EN_n(6 downto 0) <= (others => '1');
	-- leave unused switches UNCONNECTED
	--sw(15 downto 2) <= (others => '0');
		  
	-- Ignore the warnings associated with these signals
	-- Alternatively, you can create a different board implementation, 
	--   or make additional adjustments to the constraints file

	
end top_basys3_arch;
