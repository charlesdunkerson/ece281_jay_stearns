library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity destination_fsm is
    Port ( i_clk     : in  STD_LOGIC;
           i_reset   : in  STD_LOGIC;
           i_go      : in  STD_LOGIC;
           i_dest1   : in  STD_LOGIC_VECTOR (3 downto 0);
           i_dest2   : in  STD_LOGIC_VECTOR (3 downto 0);
           i_floor   : in  STD_LOGIC_VECTOR (3 downto 0);
           o_control : out STD_LOGIC_VECTOR (1 downto 0)   
		 );
end destination_fsm;


-- Write the code in a similar style as the Lesson 19 ICE (stoplight FSM version 2)
architecture Behavioral of destination_fsm is

    -- Below you create a new variable type! You also define what values that 
    -- variable type can take on. Now you can assign a signal as 
    -- "sm_floor" the same way you'd assign a signal as std_logic
	-- how would you modify this to go up to 15 floors?
	
	-- Here you create variables that can take on the values
	-- defined above. Neat!	
	signal f_current_state, f_next_state : STD_LOGIC_VECTOR (2 downto 0);

begin

	-- CONCURRENT STATEMENTS ------------------------------------------------------------------------------
	-- Next State Logic  s_floor4 when ((f_current_state = s_floor3) and (i_stop = '0' and i_up_down = '1')) else
	-- You may also use case statements here if you would prefer that implemintation.
    f_next_state <= "000" when (f_current_state = "100") or  (i_dest2 = "0000" and i_floor = i_dest1) or (i_dest1 = "0000") else
                    "001" when (f_current_state = "000") and (i_go = '1')        else
                    "010" when (f_current_state = "001") and (i_floor = i_dest1) else
                    "011" when (f_current_state = "010")                         else
                    "100" when (f_current_state = "011") and (i_floor = i_dest2) else
                    f_current_state;
	-- Output logic
	-- default is floor1
	-- you may also use the "with SEL select" signal assignment here instead of when/else
	o_control <= "00" when (f_current_state = "001" and i_floor > i_dest1) or (f_current_state = "011" and i_floor > i_dest2) else -- down
	             "10" when (f_current_state = "001" and i_floor < i_dest1) or (f_current_state = "011" and i_floor < i_dest2) else -- up
	             "11";                                                                                                             -- stop
			   
    	   
			--Fill in all possible outputs here	
	-------------------------------------------------------------------------------------------------------
	
	-- PROCESSES ------------------------------------------------------------------------------------------	
	-- State memory ------------
	register_proc : process (i_clk, i_reset)
	begin
	--What do you need to add here to make this a register with a synchronous i_reset?
	if i_reset = '1' then
               f_current_state <= "000"; --Reset state is off
               elsif (rising_edge(i_clk)) then
                   f_current_state <= f_next_state; -- next state becomes current state
               end if;
    
        end process register_proc;
	-- i_reset is active high and will return elevator to floor1
	-------------------------------------------------------------------------------------------------------
	
	



end Behavioral;